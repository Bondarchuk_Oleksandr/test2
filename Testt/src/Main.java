import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int num = (int) (Math.random()*100);
        System.out.println("Enter your name:");
        Scanner scannerName = new Scanner(System.in);
        String name = scannerName.nextLine();
        System.out.println("Let the game begin!\nEnter a number from 0 to 100:");
        Scanner scannerNum = new Scanner(System.in);
        while(true){
            int numUser = scannerNum.nextInt();
            if (num > numUser) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (num < numUser) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Congratulations, " + name + "!");
                break;
            }
        }
    }
}

